package com.kgc.cn.listener;

import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.transaction.annotation.Transactional;

public class KeyExpiredListener extends KeyExpirationEventMessageListener {
    @Autowired
    private RedisUtils redisUtils;

    public KeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Transactional
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String key = message.toString();

    }
}
