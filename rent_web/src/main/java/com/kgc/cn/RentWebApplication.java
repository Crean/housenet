package com.kgc.cn;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication
@EnableSwaggerBootstrapUI
@EnableSwagger2
public class RentWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(RentWebApplication.class, args);
    }

}
