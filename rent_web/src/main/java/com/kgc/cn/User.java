package com.kgc.cn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "用户model")
public class User implements Serializable {
    @ApiModelProperty(value = "姓名")
    String name;
    @ApiModelProperty(value = "年龄")
    String age;
}
