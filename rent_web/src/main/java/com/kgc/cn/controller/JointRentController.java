package com.kgc.cn.controller;

import com.kgc.cn.service.JointRentService;
import com.kgc.cn.utils.result.ReturnResult;
import com.kgc.cn.vo.JointRentVo;
import com.kgc.cn.vo.JointShowVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Date 2020/1/9 8:40
 * @Creat by Crane
 */
@RestController
@RequestMapping("/jointrent")
@Api(tags = "合租")
public class JointRentController {

    @Autowired
    private JointRentService jointRentService;

    @PostMapping("/add")
    @ApiOperation("添加房源信息")
    public ReturnResult add(@Valid JointRentVo jointRentVo){
        return jointRentService.add(jointRentVo);
    }

    @GetMapping("/show")
    @ApiOperation("展示多选项")
    public ReturnResult<JointShowVo> show(){
        return jointRentService.show();
    }



}
