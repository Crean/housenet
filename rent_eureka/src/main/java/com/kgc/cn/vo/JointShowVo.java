package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Date 2020/1/9 10:09
 * @Creat by Crane
 */
@Data
@ApiModel("多选项")
public class JointShowVo {
    @ApiModelProperty("房屋特色")
    private List<Characteristic> character;

    @ApiModelProperty("房屋配置")
    private List<Configuration> config;

    @ApiModelProperty("出租要求")
    private List<Demand> demand;
}
