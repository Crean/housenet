package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "筛选")
public class Screen {
    @ApiModelProperty(value = "朝向")
    private List<String> directionList;
    @ApiModelProperty(value = "出租要求")
    private List<String> demandList;
    @ApiModelProperty(value = "房源特色")
    private List<String> characteristicList;
}
