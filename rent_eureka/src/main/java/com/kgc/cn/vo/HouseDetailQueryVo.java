package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@ApiModel(value = "查询房屋model")
public class HouseDetailQueryVo implements Serializable {
    @ApiModelProperty(value = "房屋id")
    private String id;
    @ApiModelProperty(value = "用户id")
    private String uid;
    @ApiModelProperty(value = "整租：1 合租：2")
    private Integer type;
    @ApiModelProperty(value = "短租：不支持：0 一个月：1 两个月：2 三个月：3 四个月：4 五个月：5 六个月：6")
    private Integer shortrent;
    @ApiModelProperty(value = "区域")
    private String area;
    @ApiModelProperty(value = "小区名")
    private String community;
    @ApiModelProperty(value = "面积")
    private Double acreage;
    @ApiModelProperty(value = "户型")
    private String housetype;
    @ApiModelProperty(value = "月租金")
    private Integer money;
    @ApiModelProperty(value = "详细地址")
    private String address;
    @ApiModelProperty(value = "更新时间")
    private String uptime;
    @ApiModelProperty(value = "装修情况：无装修：0 普通装修：1 精装修：2 豪华装修：3")
    private Integer fitment;
    @ApiModelProperty(value = "卧室朝向：东：0 南：1 西：2 北：3")
    private Integer roomdirection;
    @ApiModelProperty(value = "朝向：东：0 南：1 西：2 北：3")
    private Integer direction;
    @ApiModelProperty(value = "房源特点")
    private Integer characteristic;
    @ApiModelProperty(value = "图片")
    private List<String> pictureList;
    @ApiModelProperty(value = "浏览次数")
    private Long pageview;
    @ApiModelProperty(value = "描述")
    private String describe;
    @ApiModelProperty(value = "卧室类型:主卧：0 次卧：1 床位：2")
    private Integer roomtype;
    @ApiModelProperty(value = "楼层")
    private String floor;
    @ApiModelProperty(value = "车位：有：0 没有：1")
    private Integer carport;
    @ApiModelProperty(value = "电梯：有：0 没有：1")
    private Integer elevator;
    @ApiModelProperty(value = "起租时间")
    private Date startrent;
    @ApiModelProperty(value = "出租要求")
    private Integer demand;
    @ApiModelProperty(value = "联系人姓名")
    private String linkman;
    @ApiModelProperty(value = "身份 个人房东：0 经纪人：1")
    private Integer status;
    @ApiModelProperty(value = "联系人手机号")
    private String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getShortrent() {
        return shortrent;
    }

    public void setShortrent(Integer shortrent) {
        this.shortrent = shortrent;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public String getHousetype() {
        return housetype;
    }

    public void setHousetype(String housetype) {
        this.housetype = housetype;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUptime() {
        return uptime;
    }

    public Integer getFitment() {
        return fitment;
    }

    public void setFitment(Integer fitment) {
        this.fitment = fitment;
    }

    public Integer getRoomdirection() {
        return roomdirection;
    }

    public void setRoomdirection(Integer roomdirection) {
        this.roomdirection = roomdirection;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(Integer characteristic) {
        this.characteristic = characteristic;
    }

    public List<String> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<String> pictureList) {
        this.pictureList = pictureList;
    }

    public Long getPageview() {
        return pageview;
    }

    public void setPageview(Long pageview) {
        this.pageview = pageview;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(Integer roomtype) {
        this.roomtype = roomtype;
    }

    public String getFloor() {
        return floor;
    }

    public Integer getCarport() {
        return carport;
    }

    public void setCarport(Integer carport) {
        this.carport = carport;
    }

    public Integer getElevator() {
        return elevator;
    }

    public void setElevator(Integer elevator) {
        this.elevator = elevator;
    }

    public Date getStartrent() {
        return startrent;
    }

    public void setStartrent(Date startrent) {
        this.startrent = startrent;
    }

    public Integer getDemand() {
        return demand;
    }

    public void setDemand(Integer demand) {
        this.demand = demand;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUptime(Date uptime) {
        long currentTime = System.currentTimeMillis();
        long updateTime = uptime.getTime();
        long intervalTime = (currentTime - updateTime) / 1000;
        if (intervalTime < 60) {
            this.uptime = intervalTime + "秒前";
        } else if (intervalTime < 60 * 60) {
            this.uptime = intervalTime / 60 + "分钟前";
        } else if (intervalTime < 60 * 60 * 24) {
            this.uptime = intervalTime / (60 * 60) + "小时前";
        } else if (intervalTime < 60 * 60 * 24 * 7) {
            this.uptime = intervalTime / (60 * 60 * 24) + "天前";
        } else {
            this.uptime = null;
        }
    }



    public void setFloor(Integer floor) {
        if (floor <= 6) {
            this.floor = "低层";
        } else if (floor > 6 && floor <= 12) {
            this.floor = "中层";
        } else {
            this.floor = "高层";
        }
    }

}
