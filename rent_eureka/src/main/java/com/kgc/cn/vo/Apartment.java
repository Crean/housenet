package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "户型")
public class Apartment {
    @ApiModelProperty(value = "出租类型")
    private List<String> typeList;

    @ApiModelProperty(value = "户型")
    private List<String> houseTypeList;
}
