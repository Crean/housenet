package com.kgc.cn.aop;

import com.kgc.cn.config.MultipleDataSourceHelper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DSSelect {
    String value() default MultipleDataSourceHelper.MASTER;
}
