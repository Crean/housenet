package com.kgc.cn.model;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table picture
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class Picture {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column picture.hid
     *
     * @mbg.generated
     */
    private String hid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column picture.picture
     *
     * @mbg.generated
     */
    private String picture;

    public Picture() {
    }

    public Picture(String hid, String picture) {
        this.hid = hid;
        this.picture = picture;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column picture.hid
     *
     * @return the value of picture.hid
     *
     * @mbg.generated
     */


    public String getHid() {
        return hid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column picture.hid
     *
     * @param hid the value for picture.hid
     *
     * @mbg.generated
     */
    public void setHid(String hid) {
        this.hid = hid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column picture.picture
     *
     * @return the value of picture.picture
     *
     * @mbg.generated
     */
    public String getPicture() {
        return picture;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column picture.picture
     *
     * @param picture the value for picture.picture
     *
     * @mbg.generated
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }
}