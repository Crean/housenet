package com.kgc.cn.controller;

import com.kgc.cn.enums.Enums;
import com.kgc.cn.service.HouseDetailService;
import com.kgc.cn.utils.returns.ReturnResult;
import com.kgc.cn.utils.returns.ReturnResultUtils;
import com.kgc.cn.vo.HouseDetailQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "房屋")
@RestController
@RequestMapping(value = "/test")
public class HouseDetailController {
    @Autowired
    private HouseDetailService houseDetailService;

    @ApiOperation(value = "房屋详情")
    @PostMapping(value = "/showDetail")
    public ReturnResult<HouseDetailQueryVo> showDetail(@ApiParam(value = "商品id") @RequestParam(value = "id") String id) {
        HouseDetailQueryVo houseDetailQueryVo = houseDetailService.queryDetail(id);
        if (houseDetailQueryVo != null) {
            return ReturnResultUtils.returnSuccess(houseDetailQueryVo);
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.HOUSE_FAIL);
    }
}
