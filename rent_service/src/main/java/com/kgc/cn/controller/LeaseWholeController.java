package com.kgc.cn.controller;


import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.kgc.cn.enums.Enums;
import com.kgc.cn.model.House;
import com.kgc.cn.model.HouseExample;
import com.kgc.cn.service.LeaseWholeServcie;
import com.kgc.cn.utils.file.FileUpdateProperties;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.utils.returnResult.ReturnResult;
import com.kgc.cn.utils.returnResult.ReturnResultUtils;
import com.kgc.cn.vo.LeaseDetailsVo;
import com.kgc.cn.vo.LeaseWholeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "房屋整租")
@RequestMapping(value = "/leaseWhole")
public class LeaseWholeController {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private LeaseWholeServcie leaseWholeServcie;


    @Autowired
    private FileUpdateProperties fileUpdateProperties;


    @PostMapping(value = "/saveMsg")
    @ApiOperation("用户退出页面保存房屋信息")
    public void saveMsg(@Valid LeaseWholeVo leaseWholeVo) {
        JSONObject jsonObject = new JSONObject();
        String leaseStr = jsonObject.toJSONString(leaseWholeVo);
        redisUtils.set(leaseWholeVo.getUid(), leaseStr);
    }


    @GetMapping(value = "/getMsg")
    @ApiOperation("用户再次访问页面判断是否有上次填写的信息")
    public ReturnResult<LeaseWholeVo> getMsg(@ApiParam("用户id") @RequestParam String uid) {
        if (redisUtils.hasKey(uid)) {
            String leaseStr = (String) redisUtils.get(uid);
            LeaseWholeVo leaseWholeVo = JSONObject.parseObject(leaseStr, LeaseWholeVo.class);
            redisUtils.del(uid);
            return ReturnResultUtils.returnSuccess(leaseWholeVo);
        }
        return null;
    }

    @ApiOperation("展示出租房屋选项")
    @GetMapping(value = "showLeaseDetails")
    public ReturnResult<LeaseDetailsVo> showLeaseDetails() {
        return ReturnResultUtils.returnSuccess(leaseWholeServcie.showLeaseDetails());
    }


    @PostMapping(value = "/addMsg")
    @ApiOperation("用户发布房屋信息")
    public ReturnResult<String> addMsg(@Valid LeaseWholeVo leaseWholeVo) {
        int flag = leaseWholeServcie.addLeaseWhole(leaseWholeVo);
        if (flag == Enums.CommonEnum.LEASE_ALEARY_FAIL.getCode()) {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.LEASE_ALEARY_FAIL);
        } else if (flag == 200) {
            return ReturnResultUtils.returnSuccess(Enums.CommonEnum.LEASE_ADD_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.LEASE_ADD_FAIL);
    }


    @ApiOperation("上传图片")
    @PostMapping(value = "/insertPicture")
    public ReturnResult<String> addDetail(@ApiParam("房屋id") @RequestParam String hid,
                                          List<MultipartFile> multipartFileImg, HttpServletRequest request) {
        String path = fileUpdateProperties.getUploadFolder() + "\\";
        String name = request.getParameter("name");
        List<String> urls = Lists.newArrayList();
        String imgUrlStr = "";
        if (multipartFileImg != null) {
            for (int i = 0; i < multipartFileImg.size(); i++) {
                //获取文件原始名
                String fileName = multipartFileImg.get(i).getOriginalFilename();
                //获取文件后缀名
                String fileType = fileName.substring(fileName.lastIndexOf("."));
                try {
                    //获取输入流
                    InputStream ins = multipartFileImg.get(i).getInputStream();
                    //文件保存目录
                    String filePath = path + "/" + name;
                    File fi = new File(filePath);
                    //如果文件夹不存在则创建
                    if (!fi.exists()) {
                        fi.mkdirs();
                    }
                    FileOutputStream out = null;
                    if (i < 3) {
                        out = new FileOutputStream(filePath + "/thumbnails" + (i + 1) + fileType);
                        urls.add("http://localhost:9001/upload/" + name + "/thumbnails" + (i + 1) + fileType);
                    } else {
                        out = new FileOutputStream(filePath + "/detail" + (i - 2) + fileType);
                        urls.add("http://localhost:9001/upload/" + name + "/detail" + (i - 2) + fileType);
                    }
                    int read = 0;
                    byte[] buffer = new byte[4096];
                    while ((read = ins.read(buffer)) != -1) {
                        out.write(buffer, 0, read); // 保存文件数据
                    }
                    out.flush();
                    out.close();
                    ins.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        leaseWholeServcie.insertHousePicture(hid, urls);
        return ReturnResultUtils.returnSuccess(Enums.CommonEnum.PICTURE_ADD_SUCCESS);
    }





}
