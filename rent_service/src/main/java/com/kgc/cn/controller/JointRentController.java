package com.kgc.cn.controller;

import com.kgc.cn.enums.FailEnum;
import com.kgc.cn.result.ReturnResult;
import com.kgc.cn.result.ReturnResultUtils;
import com.kgc.cn.vo.JointRentVo;
import com.kgc.cn.service.JointRentService;
import com.kgc.cn.utils.queryId.QueryHouseId;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @Date 2020/1/8 10:18
 * @Creat by Crane
 */
@RestController
@RequestMapping("/jointRent")
public class JointRentController {
    @Autowired
    private JointRentService jointRentService;


    @PostMapping("/add")
    @ApiOperation("添加房源信息")
    public ReturnResult add(@RequestBody JointRentVo jointRentVo) throws Exception {
        String hid = QueryHouseId.queryId();//生成房源id
        boolean state = jointRentService.add(hid, jointRentVo);
        if (!state) return ReturnResultUtils.returnFail(FailEnum.HAS_BEAN_USED);

        if (state) return ReturnResultUtils.returnSuccess(hid);

        return ReturnResultUtils.returnFail(FailEnum.PAGE_LOST);
    }


    @GetMapping("/show")
    @ApiOperation("展示多选项")
    public ReturnResult show() {
        return ReturnResultUtils.returnSuccess(jointRentService.show());
    }
}
