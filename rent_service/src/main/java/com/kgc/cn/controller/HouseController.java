package com.kgc.cn.controller;

import com.kgc.cn.service.HouseService;
import com.kgc.cn.vo.*;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/house")
public class HouseController {

    @Autowired
    private HouseService houseService;

    @PostMapping(value = "/main")
    public PageInfo mainPage(@ApiParam(value = "当前页") @RequestParam(defaultValue = "1") int pageNo,
                             @ApiParam(value = "每页数量") @RequestParam(defaultValue = "10") int pageSize,
                             @RequestBody HouseQueryVo houseQueryVo) {
        return houseService.queryHouseWithPage(pageNo, pageSize, houseQueryVo);
    }

    @PostMapping(value = "/area")
    public List<String> queryArea() {
        return houseService.queryArea();
    }

    @PostMapping(value = "/rotation")
    public List<RotationQueryVo> RotationChart() {
        return houseService.queryRotationChart();
    }

    @PostMapping(value = "/money")
    public List<MoneyScope> queryMoney() {
        return houseService.queryMoney();
    }

    @PostMapping(value = "/apartment")
    public Apartment queryApartment() {
        return houseService.queryApartment();
    }

    @PostMapping(value = "/screen")
    public Screen queryScreen() {
        return houseService.queryScreen();
    }
}
