package com.kgc.cn.service.impl;

import com.kgc.cn.mapper.*;
import com.kgc.cn.model.*;
import com.kgc.cn.vo.JointRentVo;
import com.kgc.cn.service.JointRentService;
import com.kgc.cn.utils.queryId.QueryHouseId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * @Date 2020/1/8 10:44
 * @Creat by Crane
 */
@Service
public class JointRentServiceImpl implements JointRentService {

    @Autowired
    private CharacteristicMapper characteristicMapper;

    @Autowired
    private ConfigurationMapper configurationMapper;

    @Autowired
    private DemandMapper demandMapper;

    @Autowired
    private HousesMapper housesMapper;
    @Override
    @Transactional
    public boolean add(String hid, JointRentVo jointRentVo) throws Exception {
        Houses houses = new Houses();
        BeanUtils.copyProperties(jointRentVo, houses);
        HousesExample housesExample = new HousesExample();
        housesExample.createCriteria().andAddressEqualTo(houses.getAddress());
        if (!ObjectUtils.isEmpty(housesMapper.selectByExample(housesExample))) return false;

        houses.setId(hid);
        houses.setType(2);
        houses.setUptime(QueryHouseId.getTime());
        housesMapper.insertSelective(houses);
        //房屋特点
        jointRentVo.getCharacteristic().forEach(charact -> {
            housesMapper.insertCharacteristic(charact, hid);
        });

        //房屋配置
        jointRentVo.getConfiguration().forEach(config -> {
            housesMapper.insertConfiguration(config, hid);
        });

        //出租要求
        jointRentVo.getDemand().forEach(demand -> {
            housesMapper.insertDemand(demand, hid);
        });
        return true;
    }

    @Override
    @Transactional
    public JointShowVo show() {
        JointShowVo jointShowVo = new JointShowVo();
        CharacteristicExample characteristicExample = new CharacteristicExample();
        characteristicExample.createCriteria();
        jointShowVo.setCharacter(characteristicMapper.selectByExample(characteristicExample));

        ConfigurationExample configurationExample = new ConfigurationExample();
        characteristicExample.createCriteria();
        jointShowVo.setConfig(configurationMapper.selectByExample(configurationExample));

        DemandExample demandExample = new DemandExample();
        demandExample.createCriteria();
        jointShowVo.setDemand(demandMapper.selectByExample(demandExample));

        return jointShowVo;
    }
}
