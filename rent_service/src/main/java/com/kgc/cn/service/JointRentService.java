package com.kgc.cn.service;

import com.kgc.cn.vo.JointRentVo;

/**
 * @Date 2020/1/8 10:43
 * @Creat by Crane
 */
public interface JointRentService {
    /**
     * 添加房源信息
     * @param jointRentVo
     */
    boolean add(String hid,JointRentVo jointRentVo) throws Exception;


    /**
     * 显示多选项
     * @return
     */
    JointShowVo show();

}
