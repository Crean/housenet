package com.kgc.cn.utils.returnResult;

import com.kgc.cn.enums.Enums;

/***
 * created by 北大课工场
 *
 * 统一返回工具类
 */
public class ReturnResultUtils{

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess(){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage("success");
        return returnResult;
    }
    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }
    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Integer code, String message){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }

    public static ReturnResult returnFail(Enums enums){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setCode(enums.getCode());
        returnResult.setMessage(enums.getMsg());
        return returnResult;
    }
}
