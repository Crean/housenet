package com.kgc.cn.utils.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Date 2019/12/10 15:40
 * @Creat by Crane
 */
@Component
public class WxPay {
    @Autowired
    private WxPayDto wxPayDto;

    public Map<String, String> codeUrl() throws Exception {
        Map<String, String> param = new HashMap();
        param.put("appid", wxPayDto.getAppid());
        param.put("mch_id", wxPayDto.getMchid());
        param.put("nonce_str", "");
        param.put("body", "");
        param.put("out_trade_no", "");
        param.put("total_fee", "1"/*String.valueOf(order.getoPrice())*/);
        param.put("spbill_create_ip", "192.168.1.116");
        param.put("notify_url", wxPayDto.getNotifyUrl());
        param.put("time_start", WxPayUtils.getStartTime());
        param.put("time_expire", WxPayUtils.getExpireTime());
        param.put("trade_type", "NATIVE");
        String sign = WxPayUtils.createSignature(param, wxPayDto.getKey());
        param.put("sign", sign);
        String payXml = WxPayUtils.mapToXml(param);
        String resultXml = DoPost.doPost(wxPayDto.getUnifiedorder(), payXml, 5000);
        Map<String, String> resultMap = WxPayUtils.xmlToMap(resultXml);
        return resultMap;
    }

}
