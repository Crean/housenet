package com.kgc.cn.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class MyDataSource {
    @Bean
    @ConfigurationProperties("spring.datasource.master")
    public DataSource masterDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.slave")
    public DataSource slaveDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean
    public DataSource dynamicDataSource(){
        Map<Object,Object> dataSource = new HashMap<>();
        dataSource.put(MultipleDataSourceHelper.MASTER,masterDataSource());
        dataSource.put(MultipleDataSourceHelper.SLAVE,slaveDataSource());

        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        // 定义多数据源
        dynamicDataSource.setTargetDataSources(dataSource);
        // 定义默认
        dynamicDataSource.setDefaultTargetDataSource(masterDataSource());
        return dynamicDataSource;
    }
}
